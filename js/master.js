
var link = document.querySelector('.login');
var popup = document.querySelector('.modal-content');
var closepopup = document.querySelectorAll('.modal-content-close')[0];
var closemap = document.querySelectorAll('.modal-content-close')[1];
var close  = document.querySelectorAll('.modal-content-close');
var overlay = document.querySelector('.modal-overlay');
var maplink = document.querySelector('.footer-contacts>a');
var map = document.querySelector('.modal-content-map');

link.addEventListener("click", function (event) {
  event.preventDefault();
  popup.classList.add("modal-content-show");
  overlay.classList.add("modal-content-show");
});

maplink.addEventListener("click", function (event) {
  event.preventDefault();
  map.classList.add("modal-content-show");
  overlay.classList.add("modal-content-show");  
});

for (i=0; i<close.length; i++) {
  close[i].addEventListener("click", function (event) {
    event.preventDefault();
    popup.classList.remove("modal-content-show");
    map.classList.remove("modal-content-show");
    overlay.classList.remove("modal-content-show");
  });
}

//close.forEach(function (item, i , arr) {
////  console.log(item + ': ' + item + ' (array:') + arr ')';
////  console.log("hello");
//});

//closepopup.addEventListener("click", function (event) {
//  event.preventDefault();
//  popup.classList.remove("modal-content-show");
//  map.classList.remove("modal-content-show");
//  overlay.classList.remove("modal-content-show");
//});
//
//closemap.addEventListener("click", function (event) {
//  event.preventDefault();
//  popup.classList.remove("modal-content-show");
//  map.classList.remove("modal-content-show");
//  overlay.classList.remove("modal-content-show");
//});

overlay.addEventListener("click", function (event) {
  event.preventDefault();
  popup.classList.remove("modal-content-show");
  map.classList.remove("modal-content-show");
  overlay.classList.remove("modal-content-show");
});
